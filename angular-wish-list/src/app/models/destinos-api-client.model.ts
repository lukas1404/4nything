import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viaje-state.model';
import { DestinoViaje } from './destino-viaje.model';

@Injectable()
export class DestinosApiClient {
	constructor(private store: Store<AppState>) {
	}

	add(d: DestinoViaje){
		this.store.dispatch(new NuevoDestinoAction(d));
	}

	elegir(d: DestinoViaje){
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}

}